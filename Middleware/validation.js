const {check, validationResult} = require('express-validator');
exports.validateUser = [
  check('nombre')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage('El nombre de usuario no puede estar vacío')
    .bail()
    .isLength({min: 3})
    .withMessage('Mínimo 3 caracteres')
    .bail(),
  check('email')
    .trim()
    .normalizeEmail()
    .not()
    .isEmpty()
    .withMessage('Email no válido')
    .bail(),
  check('password')
    .not()
    .isEmpty()
    .withMessage('La contaseña no puede estar vacía')
    .isLength({min: 6})
    .withMessage('La contraseña tiene que tener mínimo 6 caracteres'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({errors: errors.array()});
    next();
  },
];